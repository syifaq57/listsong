/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  Button,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {
  AdMobBanner,
  AdMobInterstitial,
  AdMobRewarded,
  PublisherBanner,
} from 'react-native-admob';
import DrawerLayout from './DrawerLayout';

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      fluidSizeIndex: 0,
    };
  }

  componentDidMount() {}

  render() {
    return (
      // <View>
      //   <ScrollView>
      //     <View style={{width: '100%', alignItems: 'center'}}>
      //       <AdMobBanner
      //         adSize="leaderboard"
      //         adUnitID="ca-app-pub-4681032570460349/1329288497"
      //         ref={el => (this._smartBannerExample = el)}
      //       />
      //       <View>
      //         <Text>eaa 1</Text>
      //       </View>
      //     </View>
      //     <View style={{marginHorizontal: 10, alignItems: 'center'}}>
      //       <PublisherBanner
      //         adSize="fullBanner"
      //         adUnitID="ca-app-pub-4681032570460349/1329288497"
      //         testDevices={[PublisherBanner.simulatorId]}
      //         onAdFailedToLoad={error => console.error(error)}
      //         onAppEvent={event => console.log(event.name, event.info)}
      //       />
      //       <View>
      //         <Text>eaa 2</Text>
      //       </View>
      //     </View>
      //   </ScrollView>
      // </View>
      <DrawerLayout />
    );
  }
}
