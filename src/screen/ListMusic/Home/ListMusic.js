/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Text,
  Image,
  StatusBar,
  KeyboardAvoidingView,
  ScrollView,
  View,
  TextInput,
  AsyncStorage,
  ActivityIndicator,
  Alert,
  BackHandler,
  Platform,
  DeviceEventEmitter,
  PushNotificationIOS,
  ImageBackground,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {
  Container,
  Header,
  Form,
  Item,
  Label,
  Input,
  Footer,
  Left,
  Right,
  Button,
  Body,
  Title,
  Card,
  CheckBox,
  Content,
} from 'native-base';

import {
  AdMobBanner,
  AdMobInterstitial,
  AdMobRewarded,
  PublisherBanner,
} from 'react-native-admob';

import {
  RectButton,
  BaseButton,
  BorderlessButton,
} from 'react-native-gesture-handler';

import colors from '../../../res/colors/index';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {color} from 'react-native-reanimated';

export default class ListMusic extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visibleAll: true,
      List_music: [
        {
          Judul: 'Sepatu',
          Artis: 'Tulus',
        },
        {
          Judul: 'Sewindu',
          Artis: 'Tulus',
        },
        {
          Judul: 'Tukar Jiwa',
          Artis: 'Tulus',
        },
      ],
    };
  }

  componentDidMount() {
    // AdMobInterstitial.setAdUnitID('ca-app-pub-4681032570460349/9136530289');
    // AdMobInterstitial.setTestDevices([AdMobInterstitial.simulatorId]);
    // AdMobInterstitial.requestAd().then(() => AdMobInterstitial.showAd());
  }

  render() {
    return (
      <Container>
        {/* <Header
          transparent>
          <View
            style={{
              width: '100%',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontSize: 22,
                fontWeight: 'bold',
                color: colors.secondary,
              }}>
              Audio Player{' '}
            </Text>
          </View>
        </Header> */}
        {/* <View style={{alignItems: 'center'}}>
            <AdMobBanner
              adSize="banner"
              adUnitID="ca-app-pub-4681032570460349/3102439124"
              ref={el => (this._smartBannerExample = el)}
            />
        </View> */}
        <Content>
          <View style={{backgroundColor: colors.blueDefault, height: '100%'}}>
            <View>
              <Image
                style={{}}
                source={require('../../../res/images/tulus.jpeg')}
              />
            </View>
          </View>
        </Content>

        <View style={{alignItems: 'center'}}>
          <PublisherBanner
            adSize="banner"
            validAdSizes={['banner', 'largeBanner', 'mediumRectangle']}
            adUnitID="ca-app-pub-4681032570460349/1329288497"
            ref={el => (this._adSizesExample = el)}
          />
        </View>
      </Container>
    );
  }
}
